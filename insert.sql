insert into Agents
values(1,'3124536754','stevenson@yahoo.com','Stevenson','Phony','15-aug-1986',56473,'Male','agent manager');
insert into Agents
values(2,'5674353212','goodhope@gmail.com','GoodHope','Chica','2-mar-1976',60618,'Male','travel agent');
insert into Agents
values(3,'3125674532','mercy@yahoo.com','Mercy','Elmhurst','19-dec-1968',34090,'Female','agent manager');



insert into Resorts 
values(1,'amanda','Monreal','12 may street','3125436754',1);
insert into Resorts
values(2,'navypier','Chicago','09 mar street','7685436785',3);
insert into Resorts
values(3,'taco','bueno','11 apr street','5647896543',3);
insert into Resorts
values(4,'palms','New_York','31 jul street','7678767788',2);
insert into Resorts
values(5,'beach','Chicago','30 aug street','3125436543',1);
insert into Resorts
values(6,'yongyang','Hong_Kong','30 oct street','9997685467',2);
insert into Resorts
values(7,'jamal','Mumbai','19 jan street','5670002100',1);
insert into Resorts
values(8,'spring','Melbourne','3 mar street','3454455675',3);



insert into Cities
values('Monreal','Canada');
insert into Cities
values('Chicago','USA');
insert into Cities
values('bueno','Mexico');
insert into Cities
values('New_York','USA');
insert into Cities
values('Hong_Kong','China');
insert into Cities
values('Mumbai','India');
insert into Cities
values('Melbourne','Australia');





insert into RAmenities
values(1,'entertainment');
insert into RAmenities
values(1,'spa');
insert into RAmenities
values(1,'wireless amenities');

insert into RAmenities
values(2,'gym');
insert into RAmenities
values(2,'wireless internet');
insert into RAmenities
values(2,'spa');
insert into RAmenities
values(2,'kayaks');
insert into RAmenities
values(2,'dry cleaning');
insert into RAmenities
values(2,'day care');
insert into RAmenities
values(2,'entertainment');

insert into RAmenities
values(3,'gym');
insert into RAmenities
values(3,'wireless internet');
insert into RAmenities
values(3,'spa');
insert into RAmenities
values(3,'kayaks');
insert into RAmenities
values(3,'dry cleaning');
insert into RAmenities
values(3,'day care');
insert into RAmenities
values(3,'entertainment');

insert into RAmenities
values(4,'entertainment');
insert into RAmenities
values(4,'dry cleaning');
insert into RAmenities
values(4,'wireless internet');
insert into RAmenities
values(4,'gym');
insert into RAmenities
values(4,'spa');

insert into RAmenities
values(5,'spa');
insert into RAmenities
values(5,'entertainment');
insert into RAmenities
values(5,'dry cleaning');

insert into RAmenities
values(6,'entertainment');
insert into RAmenities
values(6,'dry cleaning');
insert into RAmenities
values(6,'gym');
insert into RAmenities
values(6,'wireless internet');

insert into RAmenities
values(7,'wireless internet');
insert into RAmenities
values(7,'day care');
insert into RAmenities
values(7,'entertainment');

insert into RAmenities
values(8,'gym');
insert into RAmenities
values(8,'wireless internet');
insert into RAmenities
values(8,'spa');
insert into RAmenities
values(8,'dry cleaning');
insert into RAmenities
values(8,'day care');
insert into RAmenities
values(8,'entertainment');


insert into Amenities
values('gym');
insert into Amenities
values('wireless internet');
insert into Amenities
values('spa');
insert into Amenities
values('kayaks');
insert into Amenities
values('dry cleaning');
insert into Amenities
values('day care');
insert into Amenities
values('entertainment');



insert into Clients
values(1,'3125546784','gddgg@yahoo.com','Eve','Smith','19-may-63',60616,'Male');
insert into Clients
values(2,'3124567853','hgfdrtty@gmail.com','Yitzchok','Rosenberg','2-feb-80',60618,'Female');
insert into Clients
values(3,'7187654321','gsggggdd@aol.com','Helen','Garcia','5-may-73',61543,'Male');
insert into Clients
values(4,'5436789324','citta@gmail.com','Gladys','Citta','4-feb-43',76543,'Female');
insert into Clients
values(5,'7689546754','edith@yahoo.com','Edith','Albiston','21-may-83',40987,'Male');
insert into Clients
values(6,'3125467892','joanne@gmail.com','Joanne','Tang','19-oct-93',61890,'Female');
insert into Clients
values(7,'5647654321','navendu@yahoo.com','Navendu','Kumar','1-dec-59',43567,'Male');
insert into Clients
values(8,'8769087654','daria@yahoo.com','Daria','Crawford','3-apr-92',60616,'Male');

insert into Rooms
values(1,'single bed',80);
insert into Rooms
values(1,'double bed',120);

insert into Rooms
values(2,'single bed',220);
insert into Rooms
values(2,'double bed',340);
insert into Rooms
values(2,'luxury single',400);
insert into Rooms
values(2,'luxury double',600);

insert into Rooms
values(3,'single bed',200);
insert into Rooms
values(3,'double bed',300);
insert into Rooms
values(3,'luxury single',350);
insert into Rooms
values(3,'luxury double',500);

insert into Rooms
values(4,'luxury single',180);
insert into Rooms
values(4,'single bed',250);
insert into Rooms
values(4,'double bed',400);

insert into Rooms
values(5,'single bed',100);
insert into Rooms
values(5,'double bed',150);

insert into Rooms
values(6,'luxury single',140);
insert into Rooms
values(6,'single bed',200);
insert into Rooms
values(6,'double bed',250);


insert into Rooms
values(7,'single bed',60);
insert into Rooms
values(7,'double bed',120);


insert into Rooms
values(8,'single bed',150);
insert into Rooms
values(8,'double bed',200);
insert into Rooms
values(8,'luxury single',300);
insert into Rooms
values(8,'luxury double',400);


insert into bookings
values(1,1,1,1,'1-jan-06','20-jan-06','31-jan-06','single bed');
insert into bookings
values(2,2,2,2,'30-jan-06','10-jun-06','13-jun-06','luxury double');
insert into bookings
values(3,3,3,3,'21-apr-06','03-jul-06','09-jul-06','double bed');
insert into bookings
values(4,4,2,4,'3-may-06','30-jun-06','03-jul-06','luxury single');
insert into bookings
values(5,5,1,5,'27-jul-06','03-aug-06','30-aug-06','single bed');
insert into bookings
values(6,6,3,6,'2-aug-06','07-nov-06','15-nov-06','luxury single');
insert into bookings
values(7,7,2,8,'14-nov-06','30-dec-06','31-dec-06','luxury double');
insert into bookings
values(8,8,1,8,'23-nov-06','10-dec-06','20-dec-06','double bed');
insert into bookings
values(9,5,1,5,'27-aug-06','03-oct-06','10-oct-06','single bed');
insert into bookings
values(10,5,2,2,'27-oct-06','01-dec-06','03-dec-06','single bed');
insert into bookings
values(11,5,3,2,'27-jan-07','26-mar-07','30-mar-07','double bed');

insert into bookings
values(12,1,1,5,'27-apr-13','01-may-13','04-mar-13','double bed');
insert into bookings
values(13,2,2,7,'27-jan-13','26-mar-13','30-mar-13','double bed');

