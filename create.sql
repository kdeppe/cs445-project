CREATE TABLE Clients
(
CID int NOT NULL,
Phone varchar(22) NOT NULL,
Email varchar(50) NOT NULL,
FN varchar(20) NOT NULL,
LN varchar(30) NOT NULL,
DOB date NOT NULL,
Zip int NOT NULL,
Gender varchar(6) ,
Primary Key (CID),
check (Gender IN ('Male', 'Female'))
); 

CREATE TABLE Agents
(
AID int NOT NULL,
Phone varchar(22) NOT NULL,
Email varchar(50) NOT NULL,
FN varchar(20) NOT NULL,
LN varchar(30) NOT NULL,
DOB date NOT NULL,
Zip int NOT NULL,
Gender varchar(6) NOT NULL,
Pos varchar(20) NOT NULL,
Primary Key (AID),
check (Gender IN ('Male', 'Female')),
check (Pos IN ('travel agent','agent manager')));


CREATE TABLE Cities
(
City varchar(30) NOT NULL,
Country varchar(40) NOT NULL,
Primary Key (City)
);

CREATE TABLE Resorts
(
RID int NOT NULL,
RName varchar(50) NOT NULL,
City varchar(30) NOT NULL,
Address varchar(50) NOT NULL,
Phone varchar(22) NOT NULL,
Rating int NOT NULL,
FOREIGN KEY (City) REFERENCES Cities on delete cascade,
Primary Key (RID),
check (Rating IN (1,2,3))
);


CREATE TABLE Rooms
(
RID int NOT NULL, 
RType varchar(25) NOT NULL,
Price int,
FOREIGN KEY (RID) REFERENCES Resorts on delete cascade,
Primary Key (RID, RType)
);

CREATE TABLE RAmenities
(
RID int NOT NULL,
Amenity varchar(35) NOT NULL,
FOREIGN KEY(RID) REFERENCES Resorts on delete cascade,
Primary Key (RID, Amenity)
);

CREATE TABLE Amenities
(
Amenity varchar(35) NOT NULL,
Primary Key (Amenity)
);


CREATE TABLE Bookings
(
BID int NOT NULL,
CID int NOT NULL, 
AID int NOT NULL, 
RID int NOT NULL, 
BDate date NOT NULL,
ADate date NOT NULL,
DDate date NOT NULL,
RType varchar(25) NOT NULL,
FOREIGN KEY (CID) REFERENCES Clients on delete cascade,
FOREIGN KEY (AID) REFERENCES Agents on delete cascade,
FOREIGN KEY (RID) REFERENCES Resorts on delete cascade,
FOREIGN KEY (RID,RType) REFERENCES Rooms on delete cascade,
Primary Key (BID)
);


