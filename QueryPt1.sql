--Q 4
select  B.FN,B.LN,D.RName,C.BDate,C.ADate,C.DDate,C.Rtype from clients A,Agents B, Bookings C,Resorts D where A.CID = C.CID and B.AID = C.AID and A.FN = SAMPLE INPUT and A.LN=SAMPLE INPUT and C.RID = D.RID;

--Q 5
select  Rating from Resorts where RName = SAMPLE INPUT;

--Q 6
(A GIVEN AGENT)
select  sum(price*(C.DDate - C.ADate)) from Agents A,Rooms B,Bookings C where  A.AID = C.AID and B.RID = C.RID and A.FN = SAMPLE INPUT and A.LN = SAMPLE INPUT and B.RType = C.RType;

(Across All Agents)
select sum(price*(C.DDate - C.ADate)) as revenue from Rooms B,Bookings C where B.RID = C.RID and B.RType = C.RType;

(Given Country)
select  sum(price*(C.DDate - C.ADate)) from Resorts A,Rooms B,Bookings C,Cities D where  A.RID = C.RID and B.RID = C.RID and A.city = D.city and D.country = 'SAMPLE COUNTRY' and B.RType = C.RType;

(Given City)
select  sum(price*(C.DDate - C.ADate)) from Resorts A,Rooms B,Bookings C where  A.RID = C.RID and B.RID = C.RID and A.city = 'SAMPLE CITY' and B.RType = C.RType;


--Q 7
select  RName from Resorts where RID not in (Select RID from((Select RID,amenity from Resorts,(select amenity from Amenities) )minus (select * from RAmenities)));

--Q 8
(Given City)
Select  count(*) from Resorts where city = SAMPLE INPUT; 

(Given Country)
Select  count(*) from Resorts A, Cities B where country = SAMPLE INPUT and A.city = B.city;


--Q 9
select 	C.RName, (b.DDate - b.ADate) from Clients a, Bookings b,Resorts C where a.FN = SAMPLE INPUT and a.LN = SAMPLE INPUT and a.CID = b.CID and b.RID = C.RID;














		