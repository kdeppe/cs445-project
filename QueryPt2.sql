s--Q 10
SELECT * FROM Amenities WHERE Amenity LIKE ('%SAMPLE STRING%');

--Q 11
select A.CID,B.FN,B.LN from bookings A, Clients B where (sysdate - 90)< ADATE and A.CID = B.CID ;


--Q 12 A
SELECT AVG((DDate-ADate)) FROM Bookings;

--Q 12 B
SELECT AVG((DDate - ADate)) FROM Bookings NATURAL JOIN Clients GROUP BY (CID) WHERE FN = 'SAMPLE STRING' and LN = 'SAMPLE STRING';

--Q 13
 SELECT  A.FN,A.LN 
	FROM( SELECT AID, COUNT(*) AS NumBookings 
		FROM Bookings GROUP BY AID) g, Agents A 
	where g.AID = A.AID and 
	g.NumBookings >= all
		(select Numbookings 
			from (select count(*) as NumBookings 
				from Bookings group by AID)) ;


--Q 14 A
select T1.CID,A.FN,A.LN, count(*) as trips 
	from (select * from bookings where bdate >= 'min_period' and bdate <= 'max_period') T1, cLIENTS A
	where A.CID = T1.CID
	group by T1.CID,A.FN,A.LN 
		having count(*) >= all
		 	(select count(*) 
				from (select * from bookings where bdate >= 'min_period' and bdate <= 'max_period') 
				group by CID);


--Q 14 B
 select T1.CID,A.FN,A.LN, sum((DDATE-ADATE)*b.PRICE) as PRICEs 
	from (select * from bookings where bdate >= 'min_period' and bdate <= 'max_period') T1, cLIENTS A,rOOMS B
        where A.CID = T1.CID and b.RID = T1.RID and b.rtype = T1.RTYPe
	 group by T1.CID,FN,LN 
	having sum((DDate -ADate)*b.price) >= all 
		(select sum((DDATE-ADATE)*b.PRICE) 
		from (select * from bookings 
		where bdate >= 'min_period' and bdate <= 'max_period') T1, cLIENTS A,rOOMS B 
			where A.CID = T1.CID and b.RID = T1.RID and b.rtype = T1.RTYPe 
			group by T1.CID) ;


--Q 15
select A.FN,A.LN,G.num_of_clients 
	from 
		(select AID,count(*) as num_of_clients 
		from 
			(select AID,CID,count(*) 
			from bookings group
			 by AID,CID) 
		group by AID) G, Agents A
	where A.AID = G.AID 
	order by num_of_clients desc;
